## Resvg  based rendering port for OT-SVG support in FreeType

This repository contains the glue code I have written which can be plugged into FreeType at runtime to enable it to support OT-SVG fonts. The `port` directory contains the actual code and `tester` directory contains a tiny program I use to quickly render glyphs and see them.  I'll describe now how this is all supposed to be used.

## Instructions to make everything run

**Please read the *Important Notes* section below first. Then follow the instructions while keeping the notes in mind.**

1. In a new folder firstly clone the `freetype2` repo from [here](<https://git.savannah.gnu.org/cgit/freetype/freetype2.git/>). Checkout the branch `origin/GSoC-2019-moazin`. Compile the whole project by running:
```sh
sh autogen.sh
CFLAGS="-g -O0" ./configure
make -B
```
2. Clone this port but rename the whole folder from `resvg-port-freetype-otsvg` to `resvg_rendering_port`. Inside the `port` folder clone the latest `resvg` by cloning [this](https://github.com/RazrFalcon/resvg.git) repo. You must build this too by going in the `capi` folder and running:

   ``` sh
   cargo build --features="cairo-backend"
   ```

   Once you do that, just create a new folder named `build` inside the `port` directory and running `make` should compile the port. You can compile the tester program in `tester` directory by simply creating a new folder `build` and running `make` there.

3. The FreeType demo tools work with OT-SVG fonts now. To run them, clone the demo tools from [here](<https://git.savannah.gnu.org/cgit/freetype/freetype2-demos.git/>). Apply the patch I have attached `demo.patch` that you can find in this repo. Just run:

   ```sh
   git apply ../resvg_rendering_port/demo.patch
   git commit -m "Support OT-SVG" # optional
   make -B
   ```

## Important Notes

1. You'll need to have the `target/debug` folder of `resvg` in your `LD_LIBRARY_PATH`. 
2. This is only tested on Linux. You'll also need the latest version of RUST to make everything run.
3. The 3 folders namely `freetype2` , `freetype2-demos` and `resvg_rendering_port` must live in the same directory, side by side, as siblings of each other. 
4. The `tester` gets changed very quickly (mostly changes related to `glyph_index`) in `main.c`. You should open `main.c` and take a look at the commented parts to see what I mean. :-)
5. Feel free to email me at `moazinkhatri@gmail.com` in case you run into issues.

