#include <resvg_port.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_SVG_RENDERER_H
#include <drawBitmap.h>

 int main(int argc, char* argv[])
 {
    if(argc != 3)
    {
      printf("invalid args\n");
      return -1;
    }
    FT_Library library;
    FT_Error error = FT_Err_Ok;
    error = FT_Init_FreeType(&library);
    if(error){
        printf("Error initializing the library");
        return 0;
    }
    FT_Face face;
    error = FT_New_Face(library, argv[1], 0, &face);
    if(error){
        printf("Error loading the font");
        return 0;
    }
    FT_Set_Svg_Hooks( library, resvg_port_init, resvg_port_free,
                     resvg_port_render );
    /* error = FT_Set_Char_Size(face, 1000*64, 0, 72, 72); */
    error = FT_Set_Pixel_Sizes(face, 128, 128);
    if(error){
        printf("Error setting the char size");
        return 0;
    }
    /*
    unsigned long character = argv[2][0];
    unsigned int glyph_index = FT_Get_Char_Index(face, character);
    */
    /*
    unsigned long character;
    unsigned int glyph_index;
    character = FT_Get_First_Char( face, &glyph_index );
    character = FT_Get_Next_Char( face, character, &glyph_index );
    character = FT_Get_Next_Char( face, character, &glyph_index );
    character = FT_Get_Next_Char( face, character, &glyph_index );
    character = FT_Get_Next_Char( face, character, &glyph_index );
    character = FT_Get_Next_Char( face, character, &glyph_index );
    character = FT_Get_Next_Char( face, character, &glyph_index );
    character = FT_Get_Next_Char( face, character, &glyph_index );
    character = FT_Get_Next_Char( face, character, &glyph_index );
    character = FT_Get_Next_Char( face, character, &glyph_index );
    */
    unsigned int  glyph_index  = 10;

    error = FT_Load_Glyph(face, glyph_index, FT_LOAD_COLOR);
    if(error){
        printf("Error loading the glyph");
        return 0;
    }
    error = FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);
    if(error){
        printf("Error rendering the glyph");
        return 0;
    }
    FT_GlyphSlot slot = face->glyph;
    drawBitmap(slot->bitmap.rows, slot->bitmap.width, slot->bitmap.pitch, slot->bitmap.buffer);
    FT_Done_Face( face );
    FT_Done_FreeType( library );
   return 0;
 }
