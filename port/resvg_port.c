#define RESVG_CAIRO_BACKEND

#include <cairo.h>
#include <resvg.h>
#include <resvg_port.h>
#include <ft2build.h>
#include <stdlib.h>
#include <math.h>
#include FT_FREETYPE_H
#include FT_INTERNAL_TRUETYPE_TYPES_H
#include FT_BBOX_H
#include FT_SVG_RENDERER_H

  FT_Error
  resvg_port_init( FT_Library  library )
  {
    FT_Memory         memory = library->memory;
    FT_Error          error;
    Resvg_Port_State  state;

    state = (Resvg_Port_State)library->svg_renderer_state;
    resvg_init_log();
    resvg_init_options(&state->opt);
    state->opt.font_family       = "Times New Roman";
    state->opt.languages         = "en";
    state->opt.keep_named_groups = true;

    return FT_Err_Ok;
  }

  void
  resvg_port_free( FT_Library  library )
  {
    FT_Memory         memory = library->memory;
    /* Nothing to do here really! :D */
  }

  FT_Error
  resvg_port_render( FT_GlyphSlot slot, FT_BBox  outline_bbox )
  {
    resvg_rect       bbox_svg;
    resvg_rect       viewbox;
    resvg_size       size;
    FT_Error         ft_error;
    FT_SVG_Document  document;
    FT_Size_Metrics  metrics;
    FT_UShort        units_per_EM;

    unsigned char*  buffer;
    int             width;
    int             height;
    int             bitmap_left;
    int             bitmap_top;
    int             pitch;
    float           x_svg_to_out;
    float           y_svg_to_out;
    float           x_outline_to_out;
    float           y_outline_to_out;
    float           x_outline_to_svg;
    float           y_outline_to_svg;
    int             error;
    unsigned int    glyph_index;
    unsigned short  start_glyph_id;
    unsigned short  end_glyph_id;
    int             width_final;
    int             height_final;
    int             stride;

    Resvg_Port_State  state = slot->library->svg_renderer_state;


    document = (FT_SVG_Document)slot->other;

    metrics        = document->metrics;
    units_per_EM   = document->units_per_EM;
    start_glyph_id = document->start_glyph_id;
    end_glyph_id   = document->end_glyph_id;
    glyph_index    = slot->glyph_index;

    error = resvg_parse_tree_from_data( (const char*)document->svg_document,
                                      (const size_t)document->svg_document_length,
                                      &state->opt,
                                      &state->tree );
    /* TODO: (OT-SVG) Do better error handling. */
    if(error != RESVG_OK)
      return -1;


    /* TODO: (OT-SVG) For now supposing that only one glyph exists */
    size    = resvg_get_image_size( state->tree );
    viewbox = resvg_get_image_viewbox( state->tree );

    /* scale factor from SVG coordinates -> Output */
    x_svg_to_out = (float)metrics.x_ppem /(float)size.width;
    y_svg_to_out = (float)metrics.y_ppem /(float)size.height;

    /* scale factor from Outlines -> Output */
    x_outline_to_out = (float)metrics.x_ppem/(float)units_per_EM;
    y_outline_to_out = (float)metrics.y_ppem/(float)units_per_EM;

    /* scale factor from outline -> svg */
    x_outline_to_svg = (float)size.width/(float)units_per_EM;
    y_outline_to_svg = (float)size.height/(float)units_per_EM;

    bbox_svg.x        =  viewbox.x + ( outline_bbox.xMin * x_outline_to_svg );
    bbox_svg.y        =  viewbox.y - ( outline_bbox.yMax * y_outline_to_svg );
    bbox_svg.width    = ( outline_bbox.xMax - outline_bbox.xMin ) * x_outline_to_svg;
    bbox_svg.height   = ( outline_bbox.yMax - outline_bbox.yMin ) * y_outline_to_svg;

    width_final  = ceil( ( outline_bbox.xMax - outline_bbox.xMin ) * x_outline_to_out );
    height_final = ceil( ( outline_bbox.yMax - outline_bbox.yMin ) * y_outline_to_out );
    stride       = cairo_format_stride_for_width( CAIRO_FORMAT_ARGB32, width_final );

    state->surface = cairo_image_surface_create_for_data( slot->bitmap.buffer,
                                                          CAIRO_FORMAT_ARGB32,
                                                          width_final,
                                                          height_final,
                                                          stride );
    state->cr      = cairo_create(state->surface);


    if ( start_glyph_id == end_glyph_id )
    {
      /* This document contains only one glyph. So render it all
       * We need to properly translate as well to tightly fit
       * the main image.
       *
       * Here there are two approaches possible:
       *
       * 1. Let all of the scaling to be performed by cairo. Cairo will scale
       * as well as perform the translation necessary to get a tight fit.
       *
       * 2. Let resvg handle the scaling part and only do the translation here.
       *
       * The advantage of 2 is that some test SVGs for now create problems when
       * scaling is done by cairo and not by Resvg. For example the GZIP SVG
       * test font.
       *
       * Approach 1:
       *
       * cairo_scale    (state->cr, x_svg_to_out, y_svg_to_out);
       * cairo_translate( state->cr,
       *                  -1*bbox.x*x_outline_to_svg,
       *                  -1*bbox.y*y_outline_to_svg );
       * Approach 2:
       *
       * size.width = metrics.x_ppem;
       * size.height = metrics.y_ppem;
       * cairo_translate( state->cr,
       *                  -1*bbox.x*x_outline_to_out,
       *                  -1*bbox.y*y_outline_to_out );
       * Do the actual rendering
       */

      /* Approach 2 works best! TestGzip doesn't work with approach 1  */
      size.width = metrics.x_ppem; /* TODO: (OT-SVG) double check this */
      size.height = metrics.y_ppem;
      cairo_translate( state->cr,
                       -1*( bbox_svg.x - viewbox.x )*x_svg_to_out,
                       -1*( bbox_svg.y - viewbox.y )*y_svg_to_out );

      resvg_cairo_render_to_canvas( state->tree,
                                    &state->opt,
                                    size, state->cr );
    }
    else if ( start_glyph_id < end_glyph_id )
    {
      /* The document contains multiple glyphs. Let's use the ID to render
       * our specific ones.
       */
      int    length = snprintf( NULL, 0, "%u", glyph_index );
      char*  str    = malloc( 5 + length + 1 );
      strcpy( str, "glyph");
      snprintf( str + 5, length + 1, "%u", glyph_index );
      size.width = bbox_svg.width * x_svg_to_out;
      size.height = bbox_svg.height * y_svg_to_out;
      resvg_cairo_render_to_canvas_by_id( state->tree, &state->opt, size,
                                          str, state->cr );
      free(str);
    }

    /* get the data out manually so we can put it in buffer */
    /* TODO: (OT-SVG) Memory leak here */
    cairo_surface_flush( state->surface );
    width  = cairo_image_surface_get_width( state->surface );
    height = cairo_image_surface_get_height( state->surface );
    pitch  = cairo_image_surface_get_stride( state->surface );
    slot->bitmap_left = bbox_svg.x * x_svg_to_out;
    slot->bitmap_top  = bbox_svg.y * y_svg_to_out * -1;
    slot->bitmap.rows   = height;
    slot->bitmap.width  = width;
    slot->bitmap.pitch  = pitch;

    slot->bitmap.pixel_mode = FT_PIXEL_MODE_BGRA;
    slot->bitmap.num_grays  = 256;
    slot->internal->flags |= FT_GLYPH_OWN_BITMAP;
    slot->format = FT_GLYPH_FORMAT_BITMAP;

    cairo_surface_destroy( state->surface );
    cairo_destroy( state->cr );
    resvg_tree_destroy( state->tree );
    return FT_Err_Ok;
  }

  FT_UInt
  resvg_port_get_state_size( )
  {
    return sizeof( Resvg_Port_StateRec );
  }

  FT_ULong
  resvg_port_get_buffer_size( FT_GlyphSlot  slot,
                              FT_BBox       bbox )
  {
    FT_Size_Metrics  metrics;
    FT_UShort        units_per_EM;
    FT_SVG_Document  document = (FT_SVG_Document)slot->other;

    float  x_outline_to_out;
    float  y_outline_to_out;
    int    width_final;
    int    height_final;
    int    stride;

    metrics      = document->metrics;
    units_per_EM = document->units_per_EM;

    x_outline_to_out = (float)metrics.x_ppem/(float)units_per_EM;
    y_outline_to_out = (float)metrics.y_ppem/(float)units_per_EM;

    width_final  = ceil( ( bbox.xMax - bbox.xMin ) * x_outline_to_out );
    height_final = ceil( ( bbox.yMax - bbox.yMin ) * y_outline_to_out );

    stride = cairo_format_stride_for_width( CAIRO_FORMAT_ARGB32, width_final );

    return stride * height_final;
  }



