#include <cairo.h>
#include <resvg.h>
#include <ft2build.h>
#include FT_FREETYPE_H

  typedef struct Resvg_Port_StateRec_
  {
    cairo_t            *cr;
    cairo_surface_t    *surface;
    resvg_render_tree  *tree;
    resvg_options      opt;
  } Resvg_Port_StateRec;

  typedef struct Resvg_Port_StateRec_*  Resvg_Port_State;

  FT_Error
  resvg_port_init( FT_Library  library );

  void
  resvg_port_free( FT_Library  library );

  FT_Error
  resvg_port_render( FT_GlyphSlot  slot, FT_BBox  outline_bbox );

  FT_UInt
  resvg_port_get_state_size( );

  FT_ULong
  resvg_port_get_buffer_size( FT_GlyphSlot  slot,
                              FT_BBox       bbox );
